module.exports = {
  // publicPath: '/sandbox/p2020',
  css: {      
    loaderOptions: {
      sass: {
        additionalData: '@import "@/scss/variables.scss";',
        sourceMap: true,
      }
    }
  },
}