/*
Import {sleep} from xxx
usage: await sleep(xxxx)
*/
export const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

/*
import {api} from xxx
usage: fetch(`${api}/whatever`)
*/
export const api = process.env.VUE_APP_API_URL