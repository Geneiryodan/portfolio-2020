import { createRouter, createWebHashHistory } from 'vue-router'
import Home from '../views/Home.vue';
import Work from '../views/Work.vue';
import About from '../views/About.vue';
import store from '../store';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home,
    beforeEnter(to, from, next) {
      if(to.name == 'Home') {
        store.commit('SET_DARKMODE', false)
        store.commit('SET_ABOUTGRID', false)
        store.commit('REMOVE_ASIDE', false)
        next()
      }
    },
  },
  {
    path: '/about',
    name: 'About',
    component: About,
    beforeEnter(to, from, next) {
      if(to.name == 'About') {
        store.commit('SET_DARKMODE', true)
        store.commit('SET_ABOUTGRID', true)
        store.commit('REMOVE_ASIDE', false)        
          next()
      }
    }
  },
  {
    path: '/work',
    name: 'Work',
    component: Work,
    beforeEnter(to, from, next) {
      if(to.name == 'Work') {
        store.commit('SET_DARKMODE', true)
        store.commit('SET_ABOUTGRID', true)
        next()
      }
    }
  },
  {
    path: '/gitlab',
    name: 'Gitlab',
    beforeEnter() {location.href = 'https://gitlab.com/Geneiryodan/'}
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  linkActiveClass: 'is-active',
  routes
})

export default router
