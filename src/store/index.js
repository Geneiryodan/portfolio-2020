import { createStore } from 'vuex'

export default createStore({
  state: {
    darkMode: false,
    aboutGrid: false,
    removeAside: false
  },
  mutations: {
    SET_DARKMODE(state, bool) {
      state.darkMode = bool
    },

    SET_ABOUTGRID(state, bool) {
      state.aboutGrid = bool
    },

    REMOVE_ASIDE(state, bool) {
      state.removeAside = bool
    }
  },
  getters: {
    GET_DARKMODE: state => state.darkMode,
    GET_ABOUTGRID: state => state.aboutGrid,
    GET_ASIDE: state => state.removeAside
  },
  actions: {  
  },
  modules: {
  }
})
