import { createApp } from 'vue'
import App from './App.vue'
import router from './router/index.js'
import store from './store'

const Vue = createApp(App).use(store).use(router);
Vue.mount('#app')
Vue.config.devtools = true;